
$(document).ready(function(){
// Here is the array that holds the game options
var topics = [];

var btns = $('#buttons');

// Function for display gifs from the topics array
function displayGif() {

  // Empty the div of any previous content
  $("#display").empty();

  // Extracting the data-game attribute from the current button
  var game = $(this).attr("data-game");
  // Random number for the offset
  var randomNum = Math.floor(Math.random() * 10);

  // Adding the Giphy_API combined with the game keyword
  var queryURL = "https://api.giphy.com/v1/gifs/search?q=" + game + "&api_key=8nqLMD0F9BnxI0PaAWou0BVANT4fVP4Y&limit=1&offset=" + randomNum + "&rating=PG&lang=en";   
      
  // AJAX Get request
  $.ajax({
    url: queryURL,
	method: "GET"
  }).then(function(response) {
	  
    //Storing the results in a variable
	var results = response.data;

    // Loop through the response.data array
	for (var i = 0; i < results.length; i++) {
    		
	  // Creating a div for gifs with the class of "item"
      var gifDiv = $("<div class='item'>");

      // <img> div for the gifs
	  var gameGif = $("<img>");
	
	  // Adding the 'gif' class to use for Pausing later
	  gameGif.attr("class","gif");
	  
	  // Then giving the img tag an src attribute based off results[i]
	  gameGif.attr("src", results[i].images.fixed_height.url);

      // Adding the data states for still and animate, along with the JSON paths
	  gameGif.attr("data-still", results[i].images.fixed_height_still.url);
	  gameGif.attr("data-animate", results[i].images.fixed_height.url);

	  // Appending the rating paragraph and the gif div inside of the 'gifDiv'           
	  // gifDiv.append(p);
	  gifDiv.append(gameGif);
				
	  // Add gifDiv to display
	  $("#display").prepend(gifDiv);

      // Add animate/still events on click
	  $(gameGif).on("click", function() {
    
        // Grab the current state of the clicked gif
	    var state = $(this).attr("data-state");
    
        // Animation functionality
	    if (state === "still") {
	      $(this).attr("src", $(this).attr("data-animate"));
	      $(this).attr("data-state", "animate");
	    } else {
	      $(this).attr("src", $(this).attr("data-still"));
          $(this).attr("data-state", "still");
        };
      }); 

    } // End of for loop through response
  }); // End of AJAX call
}; // End of displayGif();

// CREATE BUTTONS  
function displayButtons() {
  btns.empty();
  // Using a for loop to iterate through the array.
  // and perform a function with each loop
  for (var i = 0; i < topics.length; i++) {

    // Create a new variable "gameButton" equal to <button> 
	var gameButton = $("<button>"); 

	// Assign classes to each "gameButton"  		
	gameButton.addClass("game-button game");

	// Assign data-attibutes to each "gameButton"
	// Attribute is "data-game" equal to topic[i]
	gameButton.attr("data-game", topics[i]);

	// Place the text from topics[i] and place it into the gameButton
	gameButton.text(topics[i]);

	// Add the new button to the main div
	btns.append(gameButton);
  };
}; // End of displayButtons();

// Function for handling events when the add button is clicked
$("#add-game").on("click", function(event) {
  event.preventDefault();
  
  // Grab the input from the textbox
  var newGame = $("#game-input").val().trim();
   
  // Add the new game to the topics array 
  topics.push(newGame);
  // console.log(topics);

  // Call displayButtons to process the updated array
  displayButtons();
});

// Run displayGif() when a game-button is clicked
$(document).on("click",".game-button", displayGif);

// Display initial buttons
displayButtons();

});